import regeneratorRuntime from 'regenerator-runtime'; // eslint-disable-line no-unused-vars
import React from 'react';
import { render } from 'react-dom';

import './styles/index.scss';

import App from './components/app/App';

render(<App />, document.getElementById('root'));
