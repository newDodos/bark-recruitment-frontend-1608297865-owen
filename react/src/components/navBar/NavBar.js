import React from 'react';
import { NavLink } from 'react-router-dom';

import map from 'lodash/fp/map';

export const className = 'navBar';

export const navRoutes = [
	{
		to: '/customers',
		label: 'Customers'
	},
	{
		to: '/professionals',
		label: 'Professionals'
	}
];

const NavBar = () => (

	<div className={className}>

		<ul>

			<li className={`${className}__logo`}>

				<NavLink to="/">

					<img src="https://d18jakcjgoan9.cloudfront.net/s/img/images/barklogo-dark.png!d=KY4fXZ" />

				</NavLink>

			</li>

			{map(({ to, label }) => (

				<li key={`${className}-${to}`}>

					<NavLink to={to} activeClassName={`${className}--activePage`}>{label}</NavLink>

				</li>

			), navRoutes)}

		</ul>

	</div>

);

export default NavBar;
