import React from 'react';
import PropTypes from 'prop-types';

import keys from 'lodash/fp/keys';
import pickBy from 'lodash/fp/pickBy';
import identity from 'lodash/fp/identity';

import storeShape from './utils/storeShape';

export const className = 'customersRating';

const CustomersRating = ({ customersStore = {} }) => {

	const storeKeysLength = keys(customersStore).length;
	const validStoreValuesLength = keys(pickBy(identity, customersStore)).length;
	const style = {
		backgroundColor: `rgba(71, 191, 156, ${1 * (validStoreValuesLength / storeKeysLength)})`
	};

	return (

		<div className={className} style={style}>

			<h2>Request strength rating</h2>

			<p>{validStoreValuesLength} / {storeKeysLength}</p>

		</div>

	);

};

CustomersRating.propTypes = {
	customersStore: PropTypes.shape(storeShape)
};

export default CustomersRating;
