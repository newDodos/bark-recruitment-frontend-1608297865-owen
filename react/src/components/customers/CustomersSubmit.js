import React from 'react';
import PropTypes from 'prop-types';

import storeShape from './utils/storeShape';

import FormInput from '../formInput/FormInput';
import CustomersRating from './CustomersRating';

export const className = 'customersSubmit';

const CustomersSubmit = ({ customersStore = {}, updateField }) => (

	<div className={className}>

		<FormInput
			label="Any extra information?"
			value={customersStore.extraInfo}
			onChange={value => updateField('extraInfo', value)}
		/>

		<CustomersRating customersStore={customersStore} />

	</div>

);

CustomersSubmit.propTypes = {
	customersStore: PropTypes.shape(storeShape),
	updateField: PropTypes.func
};

export default CustomersSubmit;
