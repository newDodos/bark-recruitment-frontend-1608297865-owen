import PropTypes from 'prop-types';

export default {
	service: PropTypes.string,
	location: PropTypes.string
};
