import { useReducer } from 'react';

export const initialState = {
	service: '',
	location: '',
	email: '',
	phone: '',
	name: '',
	extraInfo: ''
};

const UPDATE_CUSTOMER_FIELD = 'UPDATE_CUSTOMER_FIELD';
const updateSignUpField = (field, value) => ({
	type: UPDATE_CUSTOMER_FIELD,
	field,
	value
});

const reducer = (state, action) => {

	switch (action.type) {

		case UPDATE_CUSTOMER_FIELD:

			return {
				...state,
				[action.field]: action.value
			};

		default:

			return state;

	}

};

export default () => {

	const [ state, dispatch ] = useReducer(reducer, initialState);
	const updateField = (field, value) => dispatch(updateSignUpField(field, value));

	return [ state, updateField ];

};
