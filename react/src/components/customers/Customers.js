import React, { useState, useEffect } from 'react';
import { useParams, Redirect } from 'react-router-dom';

import getOr from 'lodash/fp/getOr';
import findIndex from 'lodash/fp/findIndex';
import map from 'lodash/fp/map';

import useCustomersStore from './utils/useCustomersStore';

import Wizard from '../wizard/Wizard';
import CustomersWhatWhere from './CustomersWhatWhere';
import CustomersContact from './CustomersContact';
import CustomersSubmit from './CustomersSubmit';

export const customerSteps = [
	{
		name: 'whatWhere',
		label: 'What & Where',
		component: CustomersWhatWhere,
		props: {}
	},
	{
		name: 'contact',
		label: 'Contact Details',
		component: CustomersContact,
		props: {}
	},
	{
		name: 'submit',
		label: 'Finish!',
		component: CustomersSubmit,
		props: {}
	}
];
export const serviceQueryUrl = 'http://localhost:4000/api/services';
export const locationQueryUrl = 'http://localhost:4000/api/locations';

export const className = 'customers';

const Customers = () => {

	const [ customersStore, updateField ] = useCustomersStore();

	const stepName = getOr('whatWhere', 'stepName', useParams());
	const stepIndex = findIndex({ name: stepName }, customerSteps);

	const [ stepToNavTo, setStepToNavTo ] = useState(null);
	useEffect(() => () => setStepToNavTo(null), [ stepToNavTo ]);
	const handleProgress = () => {

		const nextStepName = customerSteps[stepIndex + 1].name;

		setStepToNavTo(nextStepName);

	};

	const stepProps = { customersStore, updateField, serviceQueryUrl, locationQueryUrl };
	const customerStepsWithProps = map(customerStep => (

		{ ...customerStep, props: stepProps }

	), customerSteps);

	return (

		<div className={className}>

			<Wizard
				index={stepIndex}
				name={className}
				handleProgress={handleProgress}
				steps={customerStepsWithProps}
			/>

			{stepToNavTo && (<Redirect push to={`/customers/${stepToNavTo}`} />)}

		</div>

	);

};

export default Customers;
