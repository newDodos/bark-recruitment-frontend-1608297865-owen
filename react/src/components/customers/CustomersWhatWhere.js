import React from 'react';
import PropTypes from 'prop-types';

import storeShape from './utils/storeShape';

import FormInput from '../formInput/FormInput';

export const className = 'customersWhatWhere';

const CustomersWhatWhere = props => {

	const { customersStore = {}, updateField, serviceQueryUrl, locationQueryUrl } = props;

	return (

		<div className={className}>

			<FormInput
				label="What service area you looking for?"
				placeholder="e.g. Personal Trainer"
				value={customersStore.service}
				onChange={value => updateField('service', value)}
				isRequired
				queryUrl={serviceQueryUrl}
			/>

			<FormInput
				label="Where are you looking?"
				placeholder="e.g. London"
				value={customersStore.location}
				onChange={value => updateField('location', value)}
				isRequired
				queryUrl={locationQueryUrl}
			/>

		</div>

	);

};

CustomersWhatWhere.propTypes = {
	customersStore: PropTypes.shape(storeShape),
	updateField: PropTypes.func,
	serviceQueryUrl: PropTypes.string,
	locationQueryUrl: PropTypes.string
};

export default CustomersWhatWhere;
