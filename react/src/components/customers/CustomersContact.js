import React from 'react';
import PropTypes from 'prop-types';

import storeShape from './utils/storeShape';

import FormInput from '../formInput/FormInput';

export const className = 'customersContact';

const CustomersContact = ({ customersStore = {}, updateField }) => (

	<div className={className}>

		<FormInput
			label="Email"
			value={customersStore.email}
			onChange={value => updateField('email', value)}
			isRequired
		/>

		<FormInput
			label="Name"
			value={customersStore.name}
			onChange={value => updateField('name', value)}
		/>

		<FormInput
			label="Phone"
			value={customersStore.phone}
			onChange={value => updateField('phone', value)}
		/>

	</div>

);

CustomersContact.propTypes = {
	customersStore: PropTypes.shape(storeShape),
	updateField: PropTypes.func
};

export default CustomersContact;
