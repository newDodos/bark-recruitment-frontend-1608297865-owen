import React from 'react';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Redirect
} from 'react-router-dom';

import NavBar from '../navBar/NavBar';
import Customers from '../customers/Customers';
import Professionals from '../professionals/Professionals';

export const className = 'app';

const App = () => (

	<div className={className}>

		<Router>

			<NavBar />

			<Switch>

				<Route path="/customers/:stepName" component={Customers} />

				<Route path="/professionals" component={Professionals} />

				<Route path="/">

					<Redirect to="/customers/whatWhere" />

				</Route>

			</Switch>

		</Router>

	</div>

);

export default App;
