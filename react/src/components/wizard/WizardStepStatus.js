import React from 'react';
import PropTypes from 'prop-types';

import map from 'lodash/fp/map';

export const className = 'wizardStepStatus';

const stepClassName = `${className}__stepName`;
const getStepClassName = (stepLabels, stepName, index) => (

	stepLabels[index] === stepName ? `${stepClassName} ${stepClassName}--active` : stepClassName

);

const WizardStepStatus = ({ name, stepLabels, index }) => (

	<div className={className}>

		{map.convert({ cap: false })((stepName, i) => (

			<label
				key={`${className}-${name}-${i}`}
				className={getStepClassName(stepLabels, stepName, index)}
			>

				{stepName}

			</label>

		), stepLabels)}

	</div>

);

WizardStepStatus.propTypes = {
	name: PropTypes.string.isRequired,
	stepLabels: PropTypes.arrayOf(PropTypes.string),
	index: PropTypes.number
};

export default WizardStepStatus;
