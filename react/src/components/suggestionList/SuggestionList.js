import React from 'react';
import PropTypes from 'prop-types';

import map from 'lodash/fp/map';

export const className = 'suggestionList';

const SuggestionList = ({ items, onClick }) => (

	<ul className={className}>

		{map(({ value, text }) => (

			<li key={`${className}-${value}`} onClick={() => onClick(text)}>

				{text}

			</li>

		), items)}

	</ul>

);

SuggestionList.propTypes = {
	items: PropTypes.arrayOf(PropTypes.shape({
		value: PropTypes.number,
		text: PropTypes.string
	})),
	onClick: PropTypes.func
};

export default SuggestionList;
