import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import SuggestionList from '../suggestionList/SuggestionList';

export const className = 'formInput';

const FormInput = ({ label = '', placeholder, value = '', onChange, isRequired, queryUrl }) => {

	const handleChange = ({ target: { value: newValue } }) => {

		if (onChange) {

			onChange(newValue);

		}

	};

	const [ suggestions, setSuggestions ] = useState([]);
	const resetSuggestions = () => setSuggestions([]);
	useEffect(() => {

		if (queryUrl) {

			if (value.length > 2) {

				const fetchData = async () => {

					const result = await axios(`${queryUrl}?q=${value}`);

					setSuggestions(result.data);

				};

				fetchData();

			} else {

				resetSuggestions();

			}

		}

	}, [ value ]);
	const handleSuggestionClick = suggestion => {

		if (onChange) {

			onChange(suggestion);

			resetSuggestions();

		}

	};
	const shouldShowSuggestions = !!suggestions.length
		&& !(suggestions.length === 1 && suggestions[0].text === value);

	return (

		<div className={className}>

			<div className={`${className}__labelContent`}>

				{label && (

					<label className={`${className}__label`}>

						{label}

						{isRequired && (<span className={`${className}__required`}>*</span>)}

					</label>

				)}

			</div>

			<input
				type="text"
				value={value}
				onChange={handleChange}
				placeholder={placeholder}
			/>

			{shouldShowSuggestions && (

				<SuggestionList items={suggestions} onClick={handleSuggestionClick} />

			)}

		</div>

	);

};

FormInput.propTypes = {
	label: PropTypes.string,
	placeholder: PropTypes.string,
	value: PropTypes.string,
	onChange: PropTypes.func,
	isRequired: PropTypes.bool,
	queryUrl: PropTypes.string
};

export default FormInput;
